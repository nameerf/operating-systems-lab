/* Dokimastiko programma pou einai egkatesthmeno san command 
 * kai ekteleitai dinontas test_prob3 apo to termatiko. */
 
#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{	
	/* zhtame apo to xrhsth na dosei enan akeraio ton
	 * opoio meta pername os orisma apo thn sunarthsh
	 * prob3() kai mexri ton kernel opou kai 8a ton
	 * pollaplasiasoume me ton AM mas. */
	int input;
	printf("Enter an integer: ");
	scanf("%d", &input);
	/* ektyponoume to teliko apotelesma (ayto pou
	 * epistrefetai apo thn prob3()). */
	printf("Result is: %d\n", prob3(input));

	return 0;
}
