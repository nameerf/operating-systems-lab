/* Dokimastiko programma pou einai egkatesthmeno san command 
 * kai ekteleitai dinontas test_prob2 apo to termatiko. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{	
	/* desmeuoume ena tmhma mnhmhs sto opoio 8a apo8hkeusoume
	 * thn ekdosh (OS_RELEASE.OS_VERSION) tou kernel */
	char *version = (char*)malloc(sizeof(char)*20);
	
	/* "mhdenizoume" prolhptika to string */
	memset(version, '\0', sizeof(version));
	
	/* kaloume thn handler synarthsh tou system call mas */
	prob2(version);
	
	/* ektyponoume thn ekdosh tou minix */
	printf("Minix version is: %s\n", version);

	return 0;
}
