/* Boh8htikh synarthsh gia thn klhsh systhmatos PROB2. */
#include <lib.h>
#include <unistd.h>

#include <string.h>

PUBLIC int prob2(ver)
char *ver;
{
	message m;
	int r;

	m.m1_p1 = ver;
	
	r = _syscall(MM, PROB2, &m);
	
	/* antigrafoume ta perioxomena tou pinaka m3_ca1
	 * sto tmhma mnhmhs pou deixnei o deikths ver, dhl
	 * sth mnhmh pou desmeusame me malloc sto dokimastiko
	 * programma(test_prob2.c). */
	strcpy(ver, m.m3_ca1);

	return r;
}
