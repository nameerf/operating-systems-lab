/* Dokimastiko programma pou einai egkatesthmeno san command 
 * kai ekteleitai dinontas test_prob2 apo to termatiko. */

#include <lib.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include "../../servers/pm/mproc.h"

int prob2(int, void *);

int main(int argc, char *argv[])
{
	int input, proc_found;
	/* dhlonoume mia metablhth(rec), typou domhs mproc, gia na apo8hkeysoyme thn
     * eggrafh tou pinaka diergasion pou antistoixei sthn parametro(pid) pou dinei
	 * o xrhsths, an bre8ei kapoia. */
	struct mproc rec;
	
	/* pairnoume ena akeraio apo ton xrhsth pou 8a xrhsimopoih8ei os to ID ths 
	 * diergasias(pid) pou psaxnoume. */
	printf("Enter an integer: ");
	scanf("%d", &input);
	
	/* kaloume th synarthsh prob2 me orismata ton akeraio pou edose o xrhsths
     * kai th diey8ynsh ths rec. */
	proc_found = prob2(input, &rec);
	
	/* an bre8hke diergasia me pid=input typonoume kai merika alla "pedia" ths 
	 * diergasias allios typonoume analogo mhnyma. */
	if(proc_found) {
		printf("Process found! Some data of it are given below.\n");
		printf("process pid: %d\nprocess name: %s\nprocess scheduling priority(nice): %d\n", rec.mp_pid, rec.mp_name, rec.mp_nice);
	}
	else
		printf("No process found with pid=%d!\n", input);

	return 0;
}
