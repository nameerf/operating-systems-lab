#!/bin/bash

# Me thn wget katebazoume thn istoselida pou exei do8ei os orisma($1) sto bash kai thn
# apo8hkeuoume se ena arxeio me onoma saved_url.txt sto /tmp directory tou systhmatos 
# arxeion. To orisma -q kanei thn wget na mhn emfanizei thn eksodo ths sto kelufos.
wget -q -O /tmp/saved_url.txt $1

# Sthn metablhth $? apo8hkeuetai o kodikos epistrofhs ths teleytaias entolhs pou
# ektelesthke, edo ths wget. Opote elegxoume an o kodikos einai diaforos tou 0 pou 
# shmainei pos kati den phge kala (p.x. to site den yparxei h exei do8ei la8os format
# gia thn istoselida) kai ektyponoume analogo mhnyma.
if [ "$?" -ne 0 ] ; then
    echo "wget error: wget could't fetch the requested URL"
# An ola phgan kala (dhladh h wget epestrepse 0) prospa8oume na eksagoume ola ta 
# emails pou pi8anon uparxoun sthn istoselida pou apo8hkeusame sto arxeio saved_url.txt.
# Ayto to kanoume meso ths grep me orismata -E pou shmainei pos xrhsimopoioume ektetamenes
# kanonikes ekfraseis (extended regular expressions) kai me to -o pou shmainei pos an h 
# grep brei kapoio match 8a epistrepsei mono ayto kai oxi olh th grammh pou to periexei,
# dhladh sthn sygkekrimenh periptosh 8a epistrepsei mono to email. Dipla apo ta orismata 
# einai h kanonikh ekfrash (regular expression) pou xrhsimopoioume h opoia psaxnei gia 
# matches ths morfhs string1@string2.string3.string4 h ths morfhs string1@string2.string3,
# opou to ka8e string einai ena alfari8mitiko (apoteloumeno apo grammmata h/kai ari8mous).
# Na shmeio8ei oti h kanonikh ekfrash pou xrhsimopoio den lambanei ypopsin oles tis pi8anes
# ekdoxes gia ta emails alla kaluptei thn pleiopsifia ton periptoseon kai pisteuo pos einai
# arketh gia tous skopous ths askhshs.To apotelesma ths grep dioxeteuetai sthn entolh sort 
# h opoia an exoun bre8ei emails ta taksinomei alfabhtika kai telos dioxeteuoume to apotelesma
# ths sort sthn uniq h opoia an yparxei to idio email pollaples fores krataei/typonei mono to
# ena kai apaleifei ta upoloipa.
else
    grep -E -o '[[:alnum:]]+@[[:alnum:]]+\.[[:alnum:]]+(\.[[:alnum:]]+)?' < /tmp/saved_url.txt | sort | uniq
fi

# Sto telos sbhnoume to arxeio saved_url.txt sto opoio apo8hkeusame thn istoselida.
rm -f /tmp/saved_url.txt
