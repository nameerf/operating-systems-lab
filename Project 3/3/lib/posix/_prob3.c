/* Boh8htikh synarthsh gia thn klhsh systhmatos PROB3. */
#include <lib.h>
#include <unistd.h>

PUBLIC int prob3(an_int)
int an_int;
{
	message m;
	m.m1_i1 = an_int;

	_syscall(MM, PROB3, &m);

	/* epistrefoume to apotelesma */
	return(m.m1_i1);
}
