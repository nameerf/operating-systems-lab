/* Boh8htikh synarthsh gia thn klhsh systhmatos PROB3. */
#include <lib.h>
#include <unistd.h>

PUBLIC int prob3(mp)
void *mp;
{
	message m;
	/* apo8hkeyoume sthn metablhth m1_p1 tou mhnymatos
	 * ton deikth mp (afou kanoume to katallhlo cast) */
	m.m1_p1 = (char *) mp;

	return(_syscall(MM, PROB3, &m));
}
