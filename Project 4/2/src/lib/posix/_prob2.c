/* Boh8htikh synarthsh gia thn klhsh systhmatos PROB2. */
#include <lib.h>
#include <unistd.h>

PUBLIC int prob2(pid, mp)
int pid;
void *mp;
{
	message m;
	/* apo8hkeyoume sthn metablhth m1_i1 tou mhnymatos
     * ton akeraio pou edose o xrhsths os to pid ths 
	 * diergasias pros anazhthsh. */
	m.m1_i1 = pid;
	/* apo8hkeyoume sthn metablhth m1_p1 tou mhnymatos
	 * ton deikth mp (afou kanoume to katallhlo cast) */
	m.m1_p1 = (char *) mp;

	return(_syscall(MM, PROB2, &m));
}
