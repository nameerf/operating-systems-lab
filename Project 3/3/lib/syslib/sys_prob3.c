/* Boh8htikh synarthsh gia thn klhsh purhna SYS_PROB3. */
#include "syslib.h"

PUBLIC int sys_prob3(an_int, result)
int an_int;
int *result;
{
	message m;
	int r;
	m.m1_i1 = an_int;

	r = _taskcall(SYSTASK, SYS_PROB3, &m);

	/* apo8hkeuoume sth 8esh mnhmhs pou deixnei o result
	 * to apotelesma pou stal8hke apo thn synarthsh tou
	 * kernel do_prob3(). */
	*result = m.m1_i1;

	return r;
}
