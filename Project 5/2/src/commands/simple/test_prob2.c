/* Dokimastiko programma pou einai egkatesthmeno san command 
 * kai ekteleitai dinontas test_prob2 apo to termatiko. */

#include <lib.h>
#include <stdio.h>
#include <unistd.h>

int prob2(void);

int main(int argc, char *argv[])
{
	/* metablhth gia th apo8hkeush tou posostou xrhshs ths block cache */
	int cache_usage;
	
	/* kaloume thn sunarthsh prob2, pou den dexetai kapoio orisma, 
	 * kai apo8hkeuoume sthn metablhth cache_usage thn timh pou epestrepse
	 * h prob2, pou einai to pososto xrhshs ths block cache. */
	cache_usage = prob2();
	
	/* telos typonoume to pososto xrhshs ths block cache */
	printf("Block cache usage is %d%%\n", cache_usage);
	
	return 0;
}
