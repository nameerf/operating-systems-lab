/* Ylopoihsh ths klhshs purhna SYS_PROB3. Apo8hkeuei sth metablhth m1_i1
 * tou eiserxomenou mhnymatos, typou message, to apotelesma ths prakshs
 * tou pollaplasiasmou tou akeraioou me ton AM to opoio meta 8a stalei
 * aytomata piso. */
#include "../system.h"
#include "../kernel.h"

#if USE_PROB3

PUBLIC int do_prob3(m_ptr)
register message *m_ptr;	/* pointer to request message */
{
	/* pollaplasiazoume ton akeraio pou edose o xrhsths me ton AM */
	m_ptr->m1_i1 *= 2615;
	
	return(OK);
}

#endif /* USE_PROB3 */
