#!/bin/bash

# Katarxhn to scriptaki ayto apoteleitai mono apo entoles kai dioxeteueseis.
# H entolh last emfanizei mia lista me tous xrhstes pou sunde8hkan teleytaia. Epeidh
# se ayth th lista periexetai mia grammh pou ksekinaei me "wtmp" kai den mas endiaferei
# ka8os kai mia kenh grammh dioxeteuoume to apotelesma ths last sth grep opou epilegoume
# ola ta upoloipa ektos ths grammhs me to "wtmp" kai ths kenhs, dhladh mono ta usernames ton
# xrhston. Me thn entolh cut kai me to orisma -c 8 epilegoume mono tous 8 protous xarakthres,
# dhladh mono ta usernames ton xrhston apo to prohgoumeno apotelesma. Epeita taksinomoume
# to apotelesma pou pairnoume meso ths sort kai to neo apotelesma to dioxeteuoume sthn 
# uniq me orisma -c h opoia metraei tis monadikes grammes kai emfanizei os pro8ema
# ton ari8mo ton emfaniseon ths ka8e monadikhs grammhs. Telos to apotelesma pou phrame
# apo thn uniq to dioxeteuoume ksana sth sort me orismata -n kai -r h opoia taksinomei
# kata alfari8mitikh ari8mitikh timh to prohgoumeno apotelesma me to -n kai to antistrefei
# me to -r oste na einai se f8inousa seira os pros ton ari8mo ton synathseon.
last | grep wtmp -v | grep -v '^[[:space:]]*$' | cut -c -8 | sort | uniq -c | sort -n -r 
