/* Ylopoihsh ths klhshs purhna SYS_PROB2. Antigrafei ston pinaka m3_ca1
 * tou eiserxomenou mhnymatos, typou message, thn ekdosh tou purhna 
 * (OS_RELEASE + OS_VERSION) to opoio meta 8a stalei aytomata piso ston
 * manager pou ekane to kernel call. */
#include "../system.h"
#include "../kernel.h"

#include <string.h>

#if USE_PROB2

PUBLIC int do_prob2(m_ptr)
register message *m_ptr;	/* pointer to request message */
{
	/* antigrafoume(kai synenonoume) tis sta8eres OS_RELEASE kai
	 * OS_VERSION ston pinaka m3_ca1 tou mhnymatos. */
	strcpy(m_ptr->m3_ca1, OS_RELEASE);
	strcat(m_ptr->m3_ca1, ".");
	strcat(m_ptr->m3_ca1, OS_VERSION);
	
	return OK;
}

#endif /* USE_PROB2 */
