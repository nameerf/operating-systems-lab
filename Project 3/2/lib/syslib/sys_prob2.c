/* Boh8htikh synarthsh gia thn klhsh purhna SYS_PROB2. */
#include "syslib.h"

#include <string.h>

PUBLIC int sys_prob2(ver)
char *ver;
{
	message m;
	int r;
	 
	r = _taskcall(SYSTASK, SYS_PROB2, &m);
	
	strcpy(ver, m.m3_ca1);

	return r;
}
