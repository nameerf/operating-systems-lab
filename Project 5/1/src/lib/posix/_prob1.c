/* Boh8htikh synarthsh gia thn klhsh systhmatos PROB1. */
#include <lib.h>
#include <unistd.h>

PUBLIC int prob1(name)
const char *name;
{
	message m;
	/* apo8hkeyoume sthn metablhth m1_p1 tou mhnymatos to
	 * onoma tou arxeiou pou edose o xrhsths kai psaxnoume */
	m.m1_p1 = (char *)name;

	return(_syscall(FS, PROB1, &m));
}
