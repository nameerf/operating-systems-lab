/* Dokimastiko programma pou einai egkatesthmeno san command 
 * kai ekteleitai dinontas test_prob3 apo to termatiko. */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int prob3(void *);

/* orizoume mia domh typou stats */
struct stats {
	int num_active_proc;		/* ari8mos energon diergasion */
	pid_t proc_max_time;		/* to pid ths diergsasias me to megisto xrono xrhshs tou systhmatos */
	clock_t total_sys_time;		/* o synolikos xronos xrhshs tou systhmatos se ticks */
	int num_proc_wait;			/* to plh8os ton diergsion pou einai se paush (PAUSE) */
	int num_proc_pause;			/* to plh8os ton diergasion pou einai se anamonh (WAIT) */
};

int main(int argc, char *argv[])
{
	/* dhlonoume mia metablhth(stats), typou domhs stats, gia na apo8hkeysoyme
     * ta apotelesmata pou 8a paroume apo thn klhsh tou systhmatos prob3. */
	struct stats stats;
	
	/* kaloume th synarthsh prob3 me orisma th diey8ynsh ths stats. */
	prob3(&stats);

	/* typonoume ta apotelesmata pou phrame */
	printf("\n---Results---\n");
	printf("Number of (active) processes: %d\n", stats.num_active_proc);
	printf("The process with the maximum mp_child_stime has pid: %d\n", stats.proc_max_time);
	printf("Total usage time of system(in ticks): %Ld\n", stats.total_sys_time);
	printf("Number of processes in PAUSE state: %d\n", stats.num_proc_pause);
	printf("Number of processes in WAIT state: %d\n", stats.num_proc_wait);
	
	return 0;
}
