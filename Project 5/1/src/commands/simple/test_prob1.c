/* Dokimastiko programma pou einai egkatesthmeno san command 
 * kai ekteleitai dinontas test_prob1 apo to termatiko. */

#include <lib.h>
#include <limits.h>
#include <stdio.h>
#include <unistd.h>

int prob1(char *);

int main(int argc, char *argv[])
{
	/* to onoma tou arxeiou poy psaxnoume (mporei na einai full path)*/
	char filename[PATH_MAX];
	/* o ari8mos ton diergasion pou exei anoixto to arxeio */
	int num_of_processes;

	/* zhtame apo to xrhsth na mas dosei ena onoma arxeiou gia anazhthsh */
	printf("Enter a filename to search for\n(either enter the full path or a file in current directory): ");
	scanf("%s", filename);
	
	/* kaloume th synarthsh prob1 me orisma to filename pou edose o xrhsths, h opoia
     * epistrefei to plh8os ton diergasion pou exoun anoiksei to do8en arxeio(mporei na 
	 * epistrepsei -1, 0 h kapoio 8etiko akeraio). */
	num_of_processes = prob1(filename);
	
	/* An h klhsh epestrepse -1 shmainei pos den bre8hke i-node gia to arxeio kai ara
	 * to arxeio den yparxei sto disko allios typonoume to plh8os ton diergasion 
	 * pou exei anoixto to arxeio. */
	if(num_of_processes == -1)
		printf("The file %s doesn't exist in disk!\n", filename);
	else
		printf("%d process(es) has/have opened the file %s\n", num_of_processes, filename);

	return 0;
}
